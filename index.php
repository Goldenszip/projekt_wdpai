<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('index', 'DefaultController');
Router::get('pracownicy', 'DefaultController');
Router::post('login', 'SecurityController');

Router::get('o_nas', 'DefaultController');
Router::get('marki', 'DefaultController');
Router::get('kalkulator', 'DefaultController');


Router::run($path);