<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function index()
    {
        $this -> render('login');
    }

    public function pracownicy()
    {
        $this->render('pracownicy');
    }

    public function kalkulator()
    {
        $this->render('kalkulator');
    }

    public function marki()
    {
        $this->render('marki');
    }

    public function o_nas()
    {
        $this->render('o_nas');
    }


}