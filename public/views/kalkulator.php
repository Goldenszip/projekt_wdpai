<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/kalk.css">
    <script src="https://kit.fontawesome.com/a93cb3cd66.js" crossorigin="anonymous"></script>
    <title>Kalkulator PV</title>

</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="main_content">
            <div class="menu"> 
                <nav>
                    <ul>
                        <li><i class="fas fa-list-ul"></i>
                        <a href="o_nas" class="button">O nas</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="marki" class="button">Marki</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="pracownicy" class="button">Pracownicy</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="kalkulator" class="button">Kalkulator PV</a>
                        </li>


                    </ul>
                </nav> 
                

                </div>
                <div class="content">
                    <h1>PVInstal </h1>
                    <h2>Wybierz detale instalacji: </h2>
                    <p>Rodzaj instalacji</p>

                    <div class="left_content">
                        <select>
                            <option>Instalacja naziemna</option>
                            <option>Instalacja na dachówce</option>
                            <option>Instalacja na blachodachówce</option>
                            <option>Instalacja na blasze trapezowej</option>
                        </select>

                        <p>
                            Jak duża ma być instalacja ?(kW)<br>
                        </p>
                        <input type="number" name="wielkosc" min=0 id="wielkosc" value="0"><br>
                        <button type="submit" id="przelicz">Przelicz</button>



                    </div>
                    <div class="right_content">
                        Wartosc docelowa instalacji: <input type="number" id="myOutput">


                    </div>

                </div>
            </div>
        </div>

    </div>

</body>