<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/people.css">

    <script src="https://kit.fontawesome.com/a93cb3cd66.js" crossorigin="anonymous"></script>
    <title>Pracownicy</title>

</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="main_content">
            <div class="menu"> 
                <nav>
                    <ul>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="o_nas" class="button">O nas</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="marki" class="button">Marki</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="pracownicy" class="button">Pracownicy</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="kalkulator" class="button">Kalkulator PV</a>
                        </li>


                    </ul>
                </nav> 
                

                </div>
                <div class="content">
                    <h1>PVInstal</h1>
                    <section class="people">
                        <div id="project-1">
                            <img src="public/img/uploads/Johnnyb001.gif">
                            <div>
                                <h2>John Bravo</h2>
                                <p>Boss</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore.</p>
                                <div class="social-section">
                                    <i class="fab fa-instagram"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-facebook-f"></i>
                                    <i class="fas fa-globe-americas"></i>
                                </div>
                            </div>
                        </div>
                        <div id="project-2">
                            <img src="public/img/uploads/Ralph_(1).png">
                            <div>
                                <h2>Ralph Demolka</h2>
                                <p>Handlowiec</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore.</p>
                                <div class="social-section">
                                    <i class="fab fa-instagram"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-facebook-f"></i>
                                    <i class="fas fa-globe-americas"></i>
                                </div>
                            </div>
                        </div>
                        <div id="project-2">
                            <img src="public/img/uploads/Bob.jpg">
                            <div>
                                <h2>Bob Budowniczy</h2>
                                <p>Instalator</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore.</p>
                                <div class="social-section">
                                    <i class="fab fa-instagram"></i>
                                    <i class="fab fa-twitter"></i>
                                    <i class="fab fa-facebook-f"></i>
                                    <i class="fas fa-globe-americas"></i>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

</body>



