<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/marki.css">
    <script src="https://kit.fontawesome.com/a93cb3cd66.js" crossorigin="anonymous"></script>
    <title>Marki</title>

</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="main_content">
            <div class="menu"> 
                <nav>
                    <ul>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="o_nas" class="button">O nas</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="marki" class="button">Marki</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="pracownicy" class="button">Pracownicy</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="kalkulator" class="button">Kalkulator PV</a>
                        </li>


                    </ul>
                </nav> 
                

                </div>
                <div class="content">
                    <h1>PVInstal</h1>
                    <div>
                    <section class="marki">
                        <img src="public/img/uploads/longi@2x.png">
                        <img src="public/img/uploads/afore.jpg">
                        <img src="public/img/uploads/fronius@2x.jpg">
                        <img src="public/img/uploads/risen.png">
                        <img src="public/img/uploads/solaredge.jpg">
                        <img src="public/img/uploads/eaton.jpg">
                        <img src="public/img/uploads/sofar.jpg">

                    </section>


                    </div>
                </div>
            </div>
        </div>

    </div>

</body>