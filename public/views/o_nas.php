<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <script src="https://kit.fontawesome.com/a93cb3cd66.js" crossorigin="anonymous"></script>
    <title>O nas</title>

</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="main_content">
            <div class="menu"> 
                <nav>
                    <ul>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="o_nas" class="button">O nas</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="marki" class="button">Marki</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="pracownicy" class="button">Pracownicy</a>
                        </li>
                        <li><i class="fas fa-list-ul"></i>
                            <a href="kalkulator" class="button">Kalkulator PV</a>
                        </li>


                    </ul>
                </nav> 
                

                </div>
                <div class="content">
                    <h1>PVInstal</h1>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </div>
            </div>
        </div>

    </div>

</body>